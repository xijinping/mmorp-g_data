#pragma once

#include "Includes.h"
#include "Character.h"

struct Account {
    int id;
    
    std::string name;
    std::string pass;  //hash
    std::string email;
    
    bool online;
    
    uint32_t creation;
    uint32_t lastLogIn;  //unix time
    uint32_t lastLogOff;  //unix time
    uint32_t banned;  //unix time
    uint32_t expires;  //unix time
    
    std::vector<Character> characters;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Account", id, name, pass, email, online, creation, lastLogIn, lastLogOff, banned, expires);
};
