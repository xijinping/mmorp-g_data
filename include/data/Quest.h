#pragma once

#include "Includes.h"

struct Talk {
    int targetId;  //target id
    int dialogId;  //dialog id
    
    YAS_DEFINE_STRUCT_SERIALIZE("Talk", targetId, dialogId);
};

struct Go {
    Point point;
    float radius;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Go", point, radius);
};

struct Kill {
    int count;
    int npcId;  //npcId of the target(s)
    
    YAS_DEFINE_STRUCT_SERIALIZE("Kill", count, npcId);
};

struct Loot {
    int count;
    int itemId;  //itemId of the required items
    
    YAS_DEFINE_STRUCT_SERIALIZE("Loot", count, itemId);
};

struct Task {
    enum class Type {
        Talk,
        Go,
        Kill,
        Loot,
    };
    
    int id;
    Type type;
    bool completed;
    
    std::variant<Talk, Go, Kill, Loot> typeData;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Task", id, type, completed, typeData);
};

struct Quest {
    int id;
    
    uint32_t started;  //unix timestamp
    uint32_t limit;  //unix timestamp
    uint32_t completed;  //unix timestamp
    
    std::vector<Task> tasks;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Quest", id, started, limit, completed, tasks);
};
