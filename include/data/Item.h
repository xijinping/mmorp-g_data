#pragma once

#include "Includes.h"
struct Weapon {
    int attack;
    int damage;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Weapon", attack, damage);
};

struct Armor {
    int slot;
    int protection;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Armor", slot, protection);
};

struct Effect {
    enum class Type {
        Weapon,
        Armor,
    };
    
    int id;
    Type type;
    
    int value;
    int weight;
    
    std::variant<Weapon, Armor> typeData;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Effect", id, type, value, weight, typeData);
};

struct Item {
    int id;
    std::vector<Effect> effects;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Item", id, effects);
};
