#pragma once

#include "Includes.h"
#include "Chunk.h"

struct Scene {
    int id;
    int width;
    int height;
    
    std::vector<Chunk> chunks;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Scene", id, width, height, chunks);
};
