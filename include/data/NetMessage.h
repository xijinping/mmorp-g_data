#pragma once

#include "Includes.h"
#include "Account.h"
#include "Scene.h"
#include "Chunk.h"
#include "Character.h"
#include "Item.h"

struct Inventory {
    std::vector<Item> items;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Inventory", items);
};

struct ServerMessage {  //server to client structs
    enum class Type {
        GiveControl,
        
        SyncAccount,  //sync the player account
        SyncInventory,  //sync the player character inventory
        
        SyncScene,  //sync local chunks to the player
        SyncChunk,  //sync additional chunks
        
        SyncCharacter,  //sync a character
        SyncFloorItem,  //sync an item (must be on the floor)
        
        RemCharacter,
        RemFloorItem,
        
        Accept,  //accept the player input
        Reject,  //reject the player input
    };
    
    int id;
    Type type;
    
    std::variant<Account, Inventory, Scene, Chunk, Character, FloorItem, int> typeData;
    
    YAS_DEFINE_STRUCT_SERIALIZE("ServerMessage", id, type, typeData);
};

struct Connection {
    int accountId;
    std::string token;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Connection", accountId, token);
};

struct Movement {
    Point point;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Movement", point);
};

struct Dialog {
    int npcId;
    int dialogId;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Dialog", npcId, dialogId);
};

struct ClientMessage {  //client to server structs
    enum class Type {
        Connect,
        Disconnect,
        
        NewCharacter,
        UseCharacter,
        
        Move,
        Attack,
        
        Pick,
        Drop,
        
        Equip,
        Unequip,
        
        Dialog,
    };
    
    int id;
    Type type;
    
    std::variant<Connection, Character, Movement, Dialog, int> typeData;
    
    YAS_DEFINE_STRUCT_SERIALIZE("ClientMessage", id, type, typeData);
};
