#pragma once

#include "yas/serialize.hpp"
#include "yas/std_types.hpp"
#include <optional>
#include <variant>
#include <utility>
#include <vector>
#include <string>

struct Point {
    int x;
    int y;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Point", x, y);
};
