#pragma once

#include "Includes.h"
#include "Item.h"
#include "Character.h"

struct FloorItem {
    int id;
    Point pos;
    Item item;
    
    YAS_DEFINE_STRUCT_SERIALIZE("FloorItem", id, pos, item);
};

struct Chunk {
    static constexpr int size = 16;  //16x16 square
    
    int id;
    Point pos;
    
    std::vector<FloorItem> floorItems;
    std::vector<Character> characters;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Chunk", id, pos, floorItems, characters);
};
