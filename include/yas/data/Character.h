#pragma once

#include "Includes.h"
#include "Item.h"

struct AnimationState {
    int id;
    float time;
    
    YAS_DEFINE_STRUCT_SERIALIZE("AnimationState", id, time);
};

struct NPC {
    int npcId;  //useful for quests
    int state;
    
    YAS_DEFINE_STRUCT_SERIALIZE("NPC", npcId, state);
};

struct Player {
    int playerId;
    std::string name;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Player", playerId, name);
};

struct Character {
    enum class Faction {
        Friendly,
        Hostile,
    };
    
    enum class Type {
        NPC,
        Player,
    };
    
    int id;
    Type type;
    Faction faction;
    
    Point pos;
    Point dst;
    
    int hp;
    float speed;
    AnimationState animationState;
    
    std::variant<NPC, Player> typeData;
    
    std::optional<Item> head;
    std::optional<Item> body;
    std::optional<Item> legs;
    std::optional<Item> feet;
    std::optional<Item> weapon;
    
    YAS_DEFINE_STRUCT_SERIALIZE("Character", id, type, faction, pos, dst, hp, speed, animationState, typeData, head, body, legs, feet, weapon);
};
